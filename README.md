# Fisco-bcos-liens.pro

Fisco-bcos & LIENS.Pro chain united

## 联信服LIENS.Pro

**基于nodejs的人力资源区块链平台，结合FISCO-BCOS联盟链与联信服公链**


## 特别说明

LIENS.Pro是Links International Expedite Novel.Professional, 直译为“超高速国际创新联盟.职业版”，其中LIEN是法语，译为“链接”。

LIENS.Pro意义为：全球联盟职业征信服务，中文简称为“联信服”。

结合Fisco-bcos平台搭建联盟征信生态


## 资源链接

* 官方网站: http://belongto.cn/

## 安装

先安装好Fisco-bcos后，再安装Liens；

其中Fisco-bcos参考Fisco-bcos安装指南；参考：https://fisco-bcos-documentation.readthedocs.io/zh_CN/latest/docs/getstart/index.html

Liens安装好之后，需要安装好nvm后，利用nvm install 4.6.x，安装node 4.6.x版本，然后which node查看当前node的目录，利用linux的ln -s把node 4.6.x的指令绑定到node4目录上，在liens的目录下进行Node4 app.js运行测试；参考：https://github.com/Ebookcoin/ebookcoin/wiki/%E5%AE%89%E8%A3%85



本项目只保留了Fisco-bcos的tools模块，主要功能在于FISCO-BCOS/tools/contract中的demo.js，与LIENS.Pro公链形成侧链结构

**注意修改Fisco-bcos保存文件名为“mychain”,即/mychain/FISCO-BCOS/**


## 测试

运行FISCO-BCOS,至少start两个节点及以上，需要node 6.x，参考：https://fisco-bcos-documentation.readthedocs.io/zh_CN/latest/docs/getstart/index.html

运行LIENS chain，需要node 4.6.x，参考：https://github.com/Ebookcoin/ebookcoin/wiki/%E5%AE%89%E8%A3%85


这是一个LIENS.Pro的测试账号，可以用来向其他账号转账：

```
Genesis Account:
{
  "keypair": {
    "publicKey": "6ab40f67fcdff0bf4f25f58158ce3889378e2b81584a1a5b080757016ed830ed",
    "privateKey": "c3dd89317d0ecf7418258af6b9eee8d02319379d7d77ac7caa039a5abfe404806ab40f67fcdff0bf4f25f58158ce3889378e2b81584a1a5b080757016ed830ed"
  },
  "address": "4660508451220041812L",
  "secret": "AiU5S06&YaxbFL$h%aHgJaNv2fX6ho8H"
}
```



