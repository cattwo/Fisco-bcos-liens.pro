
/**
 * @file: demoHelloWorld.js
 * @author: fisco-dev
 * 
 * @date: 2017
 */
var func = function(req) {
  //block data from sidechain
  var argument = process.argv.splice(2);
  // console.log(argument);

  var Web3= require('web3');
  var config=require('../web3lib/config');
  var fs=require('fs');
  var execSync =require('child_process').execSync;
  var web3sync = require('../web3lib/web3sync');
  var BigNumber = require('bignumber.js');

  //var blocks=require('../get/getBlocks');

  if (typeof web3 !== 'undefined') {
    web3 = new Web3(web3.currentProvider);
  } else {
    web3 = new Web3(new Web3.providers.HttpProvider(config.HttpProvider));
  }

  //console.log(config);

  var filename="HelloWorld";

  //receive addressd
  // var address=fs.readFileSync(config.Ouputpath+filename+'.address','utf-8');
  var address = '0x145984103cf81a273860486ef4d0c8ba8adce98c';

  (async function(){
    
   
    var func = "set(string)";
    //var params = [JSON.stringify(obj)];
    var params = [argument[0]];
    //console.log(params[0]);
    var receipt = await web3sync.sendRawTransaction(config.account, config.privKey, address, func, params);
    
    var jsonStr = argument[0];
    // console.log(jsonStr)
    jsonStr = jsonStr.replace('{', '{"')
    var reg = new RegExp(":",'g')
    jsonStr = jsonStr.replace(reg, '":"')
    reg = new RegExp(',','g')
    jsonStr = jsonStr.replace(reg, '", "')
    jsonStr = jsonStr.replace('}', '"}')
    // var str = jsonStr.substring(1, jsonStr.length - 1);
    console.log(jsonStr)
    var obj =  JSON.parse(jsonStr); 

    //return height and txHash
    // console.log('jsonStr: ');
    // var index = str.indexOf('height:',1)+7
    // var index_end = str.indexOf(',', index)
    // console.log(index_end)
    // console.log('Block height ：'+ str.substr(index,str.indexOf(',', index)+1));
    // console.log('transaction hash ：'+receipt.transactionHash);
    console.log('Block height from json: ' + obj.height)
    console.log('transaction hash ：'+receipt.transactionHash);
    
  })()

  }

  func();