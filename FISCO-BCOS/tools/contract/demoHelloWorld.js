
/**
 * @file: demoHelloWorld.js
 * @author: fisco-dev
 * 
 * @date: 2017
 */

var Web3= require('web3');
var config=require('../web3lib/config');
var fs=require('fs');
var execSync =require('child_process').execSync;
var web3sync = require('../web3lib/web3sync');
var BigNumber = require('bignumber.js');

//var blocks=require('../get/getBlocks');


if (typeof web3 !== 'undefined') {
  web3 = new Web3(web3.currentProvider);
} else {
  web3 = new Web3(new Web3.providers.HttpProvider(config.HttpProvider));
}

//console.log(config);

var filename="HelloWorld";

//receive addressd
var address=fs.readFileSync(config.Ouputpath+filename+'.address','utf-8');

var abi=JSON.parse(fs.readFileSync(config.Ouputpath/*+filename+".sol:"*/+filename+'.abi', 'utf-8'));
var contract = web3.eth.contract(abi);
var instance = contract.at(address);


//console.log(filename+"contract address:"+address);


(async function(){

 var name=instance.get();
 //console.log("HelloWorld contract get function call first :"+name.toString());
  
  var obj = { id: '1220614864591909542',
 	height:'233'
} ;
  var func = "set(string)";
  //var params = [JSON.stringify(obj)];
  var params = [JSON.stringify(obj)];
  //console.log(params[0]);
  var receipt = await web3sync.sendRawTransaction(config.account, config.privKey, address, func, params);



  console.log('transaction hash ：'+receipt.transactionHash);
  console.log('Block height ：'+ obj.height);

  name=instance.get();
  //console.log("HelloWorld contract get function call again :"+name.toString());
  //console.log("Blocks Range is :["+blocks.blocksStart.toString()+","+blocks.blocksEnd.toString()+"]");
  //console.log("Blocks Content is :"+blocks.blocksContent.toString());
 //    console.log("============================");
 //  for(i in receipt){
 //  	console.log(i + " " + receipt[i].toString());
	// }
})()